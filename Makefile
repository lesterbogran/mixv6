OBJS = \
		bio.o\
		file.o\
		ide.o\
		kalloc.o\
		kbd.o\
		lock.o\
		page.o\
		prf.o\
		proc.o\
		string.o\
		swtch.o\
		syscall.o\
		trap.o\
		trapasm.o\
		vectors.o\
		vm.o\

FILE = \
		_shell\
		_ls\
		_calc\
		_cat\
		calc.c\

ULIB = \
		uprf.o \
		usys.o \
		ustring.o\

CC = gcc
LD = ld
OBJCOPY = objcopy
OBJDUMP = objdump
MAKEFILE_DIR := $(dir $(lastword $(MAKEFILE_LIST)))

# gcc compile options
CFLAGS = -fno-pic -static -fno-builtin -fno-strict-aliasing -fno-asynchronous-unwind-tables -O2 -Wall -MD -ggdb -m32 -Werror -fno-omit-frame-pointer
CFLAGS += $(shell $(CC) -fno-stack-protector -E -x c /dev/null >/dev/null 2>&1 && echo -fno-stack-protector)

# Disable PIE when possible (for Ubuntu 16.10 toolchain)
ifneq ($(shell $(CC) -dumpspecs 2>/dev/null | grep -e '[^f]no-pie'),)
CFLAGS += -fno-pie -no-pie
endif
ifneq ($(shell $(CC) -dumpspecs 2>/dev/null | grep -e '[^f]nopie'),)
CFLAGS += -fno-pie -nopie
endif

ASFLAGS = -m32 -gdwarf-2 -Wa,-divide

# ld options
# FreeBSD ld wants ``elf_i386_fbsd''
LDFLAGS = -m $(shell $(LD) -V | grep elf_i386 2>/dev/null | head -n 1)


mixv6.img: bootblock kernel $(FILE) mkfs
	$(MAKEFILE_DIR)mkfs $(FILE)

bootblock: bootasm.S bootmain.c
	$(CC) $(CFLAGS) -fno-pic -nostdinc -O -I. -c bootmain.c
	$(CC) $(CFLAGS) -fno-pic -nostdinc -I. -c bootasm.S
	$(LD) $(LDFLAGS) -N -e start -Ttext 0x7C00 -o bootblock.o bootasm.o bootmain.o
	$(OBJCOPY) -S -O binary -j .text bootblock.o bootblock
	$(MAKEFILE_DIR)sign.pl bootblock

kernel: $(OBJS) main.o kernel.ls
	$(LD) $(LDFLAGS) -T kernel.ls -o kernel main.o $(OBJS)
	$(OBJDUMP) -S kernel > kernel.asm

# initcode: initcode.S
# 	$(CC) $(CFLAGS) -nostdinc -I. -c initcode.S
# 	$(LD) $(LDFLAGS) -N -e start -Ttext 0x40000000 -o initcode.out initcode.o
# 	$(OBJCOPY) -S -O binary initcode.out initcode
# 	$(OBJDUMP) -S initcode.out > initcode.asm

_%: %.o $(ULIB) user.ls
	$(LD) $(LDFLAGS) -T user.ls -o $@ $^
	$(OBJDUMP) -S $@ > $*.asm

vectors.S: vectors.pl
	$(MAKEFILE_DIR)vectors.pl > vectors.S

mkfs: mkfs.c
	gcc -o mkfs mkfs.c

clean:
	rm -r *.o *.d *.asm _*

cleanall:
	rm -r *.o *.d *.asm _* vectors.S kernel bootblock mkfs mixv6.img

form:
	clang-format -i *.[ch]


