#include "proc.h"
#include "asm.h"
#include "buf.h"
#include "defs.h"
#include "file.h"
#include "macro.h"
#include "param.h"
#include "trap.h"
#include "types.h"
#include "x86.h"

char initcode[] =
    {
        // 40000000 <start>:
        0x68, 0x17, 0x00, 0x00, 0x40,            // push $0x40000017 # ($init)
        0x6a, 0x00,                              // push $0x0
        0xb8, 0x01, 0x00, 0x00, 0x00,            // mov $0x1, %eax # (SYS_exec)
        0xcd, 0x40,                              // int $0x40
                                                 // 4000000e <exit>:
        0xb8, 0x02, 0x00, 0x00, 0x00,            // mov $0x2, %eax
        0xcd, 0x40,                              // int $0x40
        0xeb, 0xf7,                              // jmp exit
                                                 // 40000017 <init>:
        0x73, 0x68, 0x65, 0x6c, 0x6c, 0x00, 0x00 // "shell"
};

/* init struct user */
void uinit()
{
    struct proc *p, *dummy;

    dummy       = &proc[0];
    dummy->stat = ZOMB;

    if ((user.procp = p = palloc()) == 0) {
        panic("uinit: palloc");
    }
    printf("Set Process Table ... Done.\n");
    inituvm(p->pgdir, initcode, sizeof(initcode));
    p->size    = sizeof(initcode);
    p->tf->esp = KERNEND + PAGESIZE;
    p->tf->eip = KERNEND; /* beginning of initcode.S */
    printf("Read root ... Done.\n");
    p->cwd  = iget(ROOTDIR);
    p->stat = RUN;
    switchuvm(p);
    printf("Initializing Process ... Done.\n");
    scr0(CR0_PG);
    printf("Turn On Paging.\n");
    printf("\nBoot Completed.\n");
    swtch(&dummy->context, p->context);
}

struct proc *palloc()
{
    struct proc *p = 0;
    char *sp;

    pushcli();

    for (int i = 0; i < NPROC; i++) {
        if (proc[i].stat == UNUSED) {
            p = &proc[i];
            memset(p, 0, sizeof(struct proc));
            break;
        }
    }
    if (p == 0) { /* process table over flow */
        popcli();
        return 0;
    }

    if ((p->pgdir = setupkvm()) == 0) {
        popcli();
        return 0;
    }

    p->stat = SET; /* don't choose this proc when sched() */
    p->pid  = user.nextpid++;
    popcli();

    if ((p->kstack = kalloc()) == 0) {
        freevm(p->pgdir);
        p->stat = UNUSED;
        return 0;
    }

    memset(p->kstack, 0, PAGESIZE);

    sp = p->kstack + STACKSIZE;
    sp -= sizeof(struct trapframe);
    p->tf = (struct trapframe *)sp;
    sp -= sizeof(struct context);
    p->context = (struct context *)sp;

    p->context->eip  = (uint32_t)trapret;
    p->tf->eflags    = FL_IF;
    p->tf->cs        = SEG_UCODE | DPL_USER;
    p->tf->ds        = SEG_UDATA | DPL_USER;
    p->tf->es        = p->tf->ds;
    p->tf->ss        = p->tf->ds;
    p->tf->eflags    = FL_IF;
    p->ofile[STDIN]  = &file[STDIN];
    p->ofile[STDOUT] = &file[STDOUT];

    return p;
}

void pfree(struct proc *p)
{
    pushcli();

    for (int fd = 2; fd < NOFILE; fd++) {
        if (p->ofile[fd]) {
            ffree(p->ofile[fd]);
            p->ofile[fd] = 0;
        }
    }

    if (p->cwd) {
        irelse(p->cwd);
        p->cwd = 0;
    }

    freevm(p->pgdir);
    kfree(p->kstack);
    p->stat = UNUSED;

    popcli();
}

int fdalloc(struct file *f)
{
    for (int fd = 0; fd < NOFILE; fd++) {
        if (user.procp->ofile[fd] == 0) {
            user.procp->ofile[fd] = f;
            return fd;
        }
    }
    return -1;
}

/* 
* swtched proc start from here.
* newproc retrun to trapret, other return to trap() that call sched().
* 
* sched process stack 
* 
* -------------------------------> %esp
* context(edi, esi, ebx, ebp)
* --------------------------------
* retrun from swtch() -> context(eip)
* --------------------------------
* return from sched()
* --------------------------------
* return from trap() -> trapret
* --------------------------------
* trapframe
* -------------------------------- interrupt
* user stack
* -------------------------------- proc.kstack(?)
* 
* 
* new process stack
* 
* -------------------------------> %esp
* context(edi, esi, ebx, ebp)
* --------------------------------
* return from swtch()  -> context(eip) -> trapret
* --------------------------------
* handmade trapframe
* -------------------------------- proc.kstack(?)
* 
*/
void sched()
{
    struct proc *newproc = 0, *oldproc;

    pushcli();

    for (int i = 0; i < NPROC; i++) {
        if (proc[(user.sched + i) % NPROC].stat == READY) {
            newproc    = &proc[(user.sched + i) % NPROC];
            user.sched = i;
        }
    }

    if (newproc == 0) {
        if (user.procp->stat != ZOMB) {
            popcli();
            return;
        }
        else { // don't return zombie proc
            for (int i = 0; i < NPROC; i++) {
                if (proc[(user.sched + i) % NPROC].stat == SLEEP) {
                    newproc    = &proc[(user.sched + i) % NPROC];
                    user.sched = i;
                }
            }
            if (newproc == 0) {
                panic("no proc.");
            }
        }
    } /* no proc for swtch */

    oldproc    = user.procp;
    user.procp = newproc;

    switchuvm(newproc);

    // printf("oldpid:%d\n", oldproc->pid);
    // printf("newpid:%d\n\n", newproc->pid);
    // printf("oldstat:%d\n", oldproc->stat);
    // printf("newstat:%d\n\n", newproc->stat);

    if (newproc->stat == READY) {
        newproc->stat = RUN;
    }
    if (oldproc->stat == RUN) {
        oldproc->stat = READY;
    }

    // printf("oldstat:%d\n", oldproc->stat);
    // printf("newstat:%d\n\n", newproc->stat);

    popcli();

    swtch(&oldproc->context, newproc->context);
}

void sleep(void *wchan)
{
    pushcli();
    user.procp->wchan = wchan;
    user.procp->stat  = SLEEP;
    popcli();
    while (user.procp->wchan) {
        sched();
    }
}

void wakeup(void *wchan)
{
    struct proc *p;
    pushcli();
    for (p = proc; p < &proc[NPROC]; p++) {
        if (p->wchan == wchan) {
            p->stat  = READY;
            p->wchan = 0;
        }
    }
    popcli();
}

void procdump()
{
    printf("pid:%d\n", user.procp->pid);
    printf("stat:%d\n", user.procp->stat);
    printf("pgdir:%x\n", user.procp->pgdir);
    printf("wchan:%x\n", user.procp->wchan);
    printf("ncli:%d\n", user.ncli);
    printf("intena:%d\n", user.intena);
    printf("eflags:%x\n", leflags());
    cli();
    while (1)
        ;
}