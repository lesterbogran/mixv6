#include "syscall.h"
#include "defs.h"
#include "elf.h"
#include "file.h"
#include "macro.h"
#include "page.h"
#include "param.h"
#include "proc.h"
#include "trap.h"
#include "types.h"
#include "x86.h"

extern int sys_exec(void);
extern int sys_exit(void);
extern int sys_read(void);
extern int sys_write(void);
extern int sys_open(void);
extern int sys_close(void);
extern int sys_wait(void);

static int (*syscalls[])(void) = {
    [SYS_exec] sys_exec,
    [SYS_exit] sys_exit,
    [SYS_read] sys_read,
    [SYS_write] sys_write,
    [SYS_open] sys_open,
    [SYS_close] sys_close,
    [SYS_wait] sys_wait,
};

int argint(int argc)
{
    return *(int *)((user.procp->tf->esp) + 4 * (argc + 1));
}

char *argptr(int argc)
{
    int n = argint(argc);
    return n > KERNEND ? (char *)n : 0;
}

struct file *argfd(int argc)
{
    int fd = argint(argc);
    if (fd < 0 || fd >= NOFILE) {
        return 0;
    }
    return user.procp->ofile[fd];
}

int syscall(uint32_t num)
{
    if (num > 0 && num < NELEM(syscalls) && syscalls[num]) {
        return syscalls[num]();
    }
    else {
        printf("pid %d: unknown sys call %d\n", user.procp->pid, num);
        return -1;
    }
}

int sys_read(void)
{
    struct file *f = argfd(0);
    char *dst      = argptr(1);
    int size       = argint(2), r;

    if (f == 0 || dst == 0 || size < 0) {
        return -1;
    }
    acquire(&f->inode->lock);
    if ((r = readi(f->inode, dst, f->offset, size)) > 0) {
        f->offset += r;
    }
    release(&f->inode->lock);

    return r;
}

int sys_write(void)
{
    struct file *f = argfd(0);
    char *src      = argptr(1);
    int size       = argint(2), r;

    if (f == 0 || src == 0 || size < 0) {
        return -1;
    }

    acquire(&f->inode->lock);
    if ((r = writei(f->inode, src, f->offset, size)) > 0) {
        f->offset += r;
    }
    release(&f->inode->lock);

    return r;
}

int sys_open(void)
{
    char *path = argptr(0);
    struct inode *ip;
    struct file *f;
    int fd;

    if ((ip = namei(path)) == 0) {
        return -1;
    }

    if ((f = falloc()) == 0 || (fd = fdalloc(f)) < 0) {
        if (f) {
            ffree(f);
        }
        return -1;
    }

    f->inode  = ip;
    f->offset = 0;
    return fd;
}

int sys_close(void)
{
    int fd = argint(0);
    if (fd < 2 || fd >= NOFILE) {
        return -1;
    }
    ffree(user.procp->ofile[fd]);
    user.procp->ofile[fd] = 0;
    return 0;
}

int sys_exec(void)
{
    char *path = argptr(0);
    struct elfhdr elf;
    struct proghdr ph;
    struct inode *ip;
    struct proc *procp = 0;
    uint32_t baseaddr;
    pte_t *pte;

    if ((ip = namei(path)) == 0) {
        return -1;
    }
    acquire(&ip->lock);
    if (readi(ip, (char *)&elf, 0, sizeof(elf)) != sizeof(elf))
        goto bad;
    if (elf.magic != ELF_MAGIC)
        goto bad;
    if ((procp = palloc()) == 0)
        goto bad;
    /* Load program into memory. */
    baseaddr = KERNEND;
    for (int i = 0, off = elf.phoff; i < elf.phnum; i++, off += sizeof(ph)) {
        if (readi(ip, (char *)&ph, off, sizeof(ph)) != sizeof(ph))
            goto bad;
        if (ph.type != ELF_PROG_LOAD)
            continue;
        if (ph.memsz < ph.filesz)
            goto bad;
        if (ph.vaddr + ph.memsz < ph.vaddr)
            goto bad;
        if ((baseaddr = allocuvm(procp->pgdir, baseaddr, ph.vaddr + ph.memsz)) == 0)
            goto bad;
        if (ph.vaddr % PAGESIZE != 0)
            goto bad;
        if (loaduvm(procp->pgdir, (char *)ph.vaddr, ip, ph.off, ph.filesz) < 0)
            goto bad;
    }
    release(&ip->lock);
    ip = 0;

    // Allocate two pages at the next page boundary.
    // Make the first inaccessible.  Use the second as the user stack.
    baseaddr = PGROUNDUP(baseaddr);
    if ((baseaddr = allocuvm(procp->pgdir, baseaddr, baseaddr + 2 * PAGESIZE)) == 0)
        goto bad;
    pte = walkpgdir(procp->pgdir, (baseaddr - 2 * PAGESIZE), 0);
    *pte &= ~PTE_U;

    procp->size    = baseaddr - KERNEND;
    procp->pproc   = user.procp;
    procp->cwd     = idup(user.procp->cwd);
    procp->tf->esp = baseaddr - 0x40;
    procp->tf->eip = elf.entry;
    procp->stat    = READY;

    return 0;

bad:
    if (procp)
        pfree(procp);
    if (ip)
        release(&ip->lock);
    return -1;
}

int sys_exit(void)
{
    pushcli();

    user.procp->stat = ZOMB;

    if (user.procp->pproc)
        wakeup(user.procp->pproc);

    popcli();

    sched();

    panic("zombie exit");
    return 0;
}

int sys_wait(void)
{
    sleep(user.procp);
    for (int i = 0; i < NPROC; i++) {
        if (proc[i].pproc == user.procp && proc[i].stat == ZOMB) {
            pfree(&proc[i]);
        }
    }
    return 0;
}