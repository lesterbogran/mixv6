#pragma once

#include "types.h"

/* Long-term locks for processes */
struct lock {
    uint32_t locked;    /* Is the lock held? */
    struct proc *procp; /* Process holding lock */
};