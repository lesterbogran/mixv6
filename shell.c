#include "ulib.h"

int isbackground(char *cmd, char pat, int size);

int main(void)
{
    char cmd[128];
    int bgtaskflag;

    printf("\n\nShell Ready.\n");

    while (1) {
        printf("> ");
        read(0, cmd, sizeof(cmd));
        if (*cmd == 0) {
            continue;
        }
        bgtaskflag = isbackground(cmd, ' ', sizeof(cmd));
        if (exec(cmd) < 0) {
            printf("exec: fail - %s\n", cmd);
        }
        else if (!bgtaskflag) {
            wait();
        }
        memset(cmd, 0, sizeof(cmd));
    }
    return 0;
}

int isbackground(char *cmd, char pat, int size)
{
    for (int i = 0; i < size; i++) {
        if (cmd[i] == pat) {
            if (cmd[i + 1] == '&') {
                memset(&cmd[i], 0, size - 1);
                return 1;
            }
            memset(&cmd[i], 0, size - 1);
            return 0;
        }
    }
    return 0;
}
