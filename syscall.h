// System call numbers
#define SYS_exec 1
#define SYS_exit 2
#define SYS_read 3
#define SYS_write 4
#define SYS_open 5
#define SYS_close 6
#define SYS_wait 7

#define T_SYSCALL 64 // system call