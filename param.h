#pragma once

/* memory map */
#define CGAADDR 0xB8000    /* CGA memory */
#define EXTMEM 0x100000    /* [0,1M] is I/O space that need writable */
#define KERNEND 0x40000000 /* kernel space is [1M, 1G] */
#define USERSTART 0x40000000
#define USEREND 0xFFC00000 /* user space end */

#define MEMSIZE 0x20000000            /* memory size is 512MB */
#define PAGESIZE 0x1000               /* page size is 4KB */
#define STACKSIZE 0x1000              /* kstack size */
#define SECTSIZE 512                  /* sector size is 512Byte */
#define DISKSIZE 0x400000             /* 4MB, max size is 128GB */
#define NSECTOR (DISKSIZE / SECTSIZE) /* how many sectors in disk */
