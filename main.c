#include "defs.h"
#include "x86.h"

int main(void)
{
    cli();
    consoleinit(); /* Console (CGA) */
    seginit();     /* Segment Descriptor Table */
    kbdinit();     /* Keyboard */

    kinit();   /* physical memory space */
    kvminit(); /* kernel virtual memory space */

    idtinit(); /* Interrupt Descriptor Table */
    picinit(); /* Programmable Interrupt Controller */
    pitinit(); /* Programmable Interval Timer */

    sti();
    uinit(); /* init process */

    panic("fail init process.");
}
