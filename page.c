#include "page.h"
#include "defs.h"
#include "macro.h"
#include "types.h"

pte_t *walkpgdir(uint32_t *pgdir, uint32_t vaddr, int isalloc)
{
    if (pgdir == 0) {
        panic("walkpgdir");
    }

    pde_t *pde = &pgdir[PDX(vaddr)];
    pte_t *pgtab;

    if (*pde & PTE_P) {
        pgtab = (pte_t *)PTE_ADDR(*pde);
    }
    else {
        if (!isalloc || (pgtab = (pte_t *)kalloc()) == 0) {
            return 0;
        }
        memset(pgtab, 0, PAGESIZE);
        *pde = (uint32_t)pgtab | PTE_P | PTE_W | PTE_U;
    }
    return &pgtab[PTX(vaddr)];
}

int mappages(uint32_t *pgdir, uint32_t vaddr, uint32_t size, uint32_t paddr, uint32_t flags)
{
    uint32_t addr, last;
    pte_t *pte;

    if (pgdir == 0) {
        panic("mappages");
    }

    addr = PGROUNDDOWN(vaddr);
    last = PGROUNDDOWN(vaddr + size - 1);

    for (; addr <= last; addr += PAGESIZE, paddr += PAGESIZE) {
        if ((pte = walkpgdir(pgdir, addr, 1)) == 0) {
            return -1;
        }
        if (*pte & PTE_P) {
            panic("mappages");
        }
        *pte = paddr | flags | PTE_P;
    }

    return 0;
}