#pragma once

#include "types.h"

/* parameter for paging */
#define NPDENT 1024 /* number of page directory entry */
#define NPTENT 1024 /* number of page table entry */

/* Page table/directory entry flags. */
#define PTE_P 0x001  /* Present */
#define PTE_W 0x002  /* Writeable */
#define PTE_U 0x004  /* User */
#define PTE_PS 0x080 /* Page Size */