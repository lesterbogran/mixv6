#include "trap.h"
#include "asm.h"
#include "defs.h"
#include "page.h"
#include "proc.h"
#include "syscall.h"
#include "x86.h"

// Interrupt descriptor table (shared by all CPUs).
struct gatedesc idt[256];
static uint16_t idtr[3];
extern uint32_t vectors[]; // in vectors.S: array of 256 entry pointers
static uint32_t tick;

void idtinit()
{
    for (int i = 0; i < 256; i++)
        SETGATE(idt[i], 0, SEG_KCODE, vectors[i], 0);
    SETGATE(idt[T_SYSCALL], 1, SEG_KCODE, vectors[T_SYSCALL], DPL_USER);

    idtr[0] = sizeof(idt) - 1;
    idtr[1] = (uint32_t)idt;
    idtr[2] = (uint32_t)idt >> 16;
    asm volatile("lidt (%0)" ::"r"(idtr));
    printf("Set IDT ... Done.\n");
}

void picinit()
{
    outb(PIC0_DATA, PIC_MASKALL);
    outb(PIC1_DATA, PIC_MASKALL);
    outb(PIC0_CMD, PIC_ICW1);
    outb(PIC1_CMD, PIC_ICW1);
    outb(PIC0_DATA, PIC0_ICW2);
    outb(PIC1_DATA, PIC1_ICW2);
    outb(PIC0_DATA, PIC0_ICW3);
    outb(PIC1_DATA, PIC1_ICW3);
    outb(PIC0_DATA, PIC_ICW4);
    outb(PIC1_DATA, PIC_ICW4);
    outb(PIC0_DATA, PIC0_MASK);
    outb(PIC1_DATA, PIC1_MASK);
    printf("Init 8259A(PIC) ... Done.\n");
}

void pitinit()
{
    outb(PIT_CTRL, 0x34); /* set intterupt */
    outb(PIT_CNT0, FREQ_MAGIC & 0xFF);
    outb(PIT_CNT0, FREQ_MAGIC >> 8);
    printf("Init 8253(PIT) ... Done.\n");
}

void piceoi(uint32_t no)
{
    if (no >= 8) {
        outb(PIC1_CMD, PIC_EOI);
    }
    outb(PIC0_CMD, PIC_EOI);
}

void trap(struct trapframe *tf)
{
    if (tf->trapno == T_SYSCALL) {
        memmove(tf, user.procp->tf, sizeof(struct trapframe));
        tf->eax = syscall(tf->eax);
        return;
    }

    switch (tf->trapno) {
    case T_PGFLT:
        panic("pagefault");
        break;

    case T_IRQ0 + IRQ_TIMER:
        tick++;
        user.procp->cpu++;
        if (tick % 100 == 0) {
            sched();
        }
        piceoi(IRQ_TIMER);
        break;

    case T_IRQ0 + IRQ_KBD:
        kbdintr();
        piceoi(IRQ_KBD);
        break;

    case T_IRQ0 + IRQ_IDE:
        ideintr();
        piceoi(IRQ_IDE);
        break;

    default:
        printf("unknown trap: No.%d\n", tf->trapno);
        panic("trap");
        break;
    }
}