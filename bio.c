#include "buf.h"
#include "defs.h"
#include "file.h"
#include "macro.h"
#include "page.h"
#include "types.h"

struct buf *bget(uint32_t blockno)
{
    pushcli();
    /* find cache */
    for (int i = 0; i < NBUF; i++) {
        if (buf[i].blockno == blockno) {
            buf[i].refcnt++;
            popcli();
            return &buf[i];
        }
    }
    /* no cache */
    for (int i = 0; i < NBUF; i++) {
        if (buf[i].refcnt == 0 && (buf[i].flags & B_DIRTY) == 0) {
            buf[i].blockno = blockno;
            buf[i].flags   = 0;
            buf[i].refcnt  = 1;
            popcli();
            return &buf[i];
        }
    }

    panic("bget: no buffers");
    return 0;
}

struct buf *bread(uint32_t blockno)
{
    struct buf *b = bget(blockno);
    if (b->refcnt > 1) {
        return b;
    }
    acquire(&b->lock);
    if ((b->flags & B_VALID) == 0) {
        iderw(b);
    }
    release(&b->lock);
    return b;
}

void bwrite(struct buf *b)
{
    acquire(&b->lock);
    if (!holding(&b->lock)) {
        panic("bwrite");
    }
    b->flags |= B_DIRTY;
    iderw(b);
    release(&b->lock);
}

void brelse(struct buf *b)
{
    b->refcnt--;
}