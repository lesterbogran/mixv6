#include "defs.h"
#include "lock.h"
#include "macro.h"
#include "page.h"
#include "param.h"
#include "proc.h"
#include "types.h"
#include "x86.h"

extern char kernel_end[]; /* first address after kernel loaded from ELF file */
extern char rodata_end[]; /* address of finish read only data */

struct run {
    struct run *next;
};

struct run *coremap;

void kinit()
{
    printf("Kernel Size is %p\n", kernel_end);
    printf("Memory Size is %x\n", MEMSIZE);
    printf("Init Memory ");
    char *p = (char *)PGROUNDUP((uint32_t)kernel_end);
    for (; p < (char *)MEMSIZE; p += PAGESIZE) {
        kfree(p);
        if ((uint32_t)p % (MEMSIZE / 4) == 0) {
            printf(".");
        }
    }
    printf(" Done\n");
}

/* allocate 4KB physical memory */
void *kalloc()
{
    struct run *r;

    pushcli();
    r = coremap;
    if (r) {
        coremap = r->next;
    }
    popcli();
    return (void *)r; /* return 0 if can't allocated */
}

void kfree(char *addr)
{
    struct run *r;

    if ((uint32_t)addr % PAGESIZE || addr < kernel_end || (uint32_t)addr >= MEMSIZE) {
        panic("kfree");
    }
    /* remove data in page for next use */
    memset(addr, 0, PAGESIZE);
    pushcli();
    r       = (struct run *)addr;
    r->next = coremap;
    coremap = r;
    popcli();
}