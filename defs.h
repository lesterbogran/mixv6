#pragma once

#include "types.h"

/* bio.c */
struct buf *bget(uint32_t blockno);
struct buf *bread(uint32_t blockno);
void bwrite(struct buf *);
void brelse(struct buf *b);

/* file.c */
struct file *falloc();
void ffree(struct file *f);
struct inode *iget(uint32_t inum);
void irelse(struct inode *ip);
struct inode *idup(struct inode *ip);
int readi(struct inode *ip, char *dst, uint32_t offset, uint32_t size);
int writei(struct inode *ip, char *src, uint32_t offset, uint32_t size);
struct inode *namei(char *path);

/* ide.c*/
void iderw(struct buf *);
void ideintr();

/* kalloc.c */
void kinit();           /* init physical memory */
void *kalloc();         /* allocate physical memory by 4KB */
void kfree(char *addr); /* free physical memory */

/* kbd.c */
void kbdinit();
void kbdintr();

/* lock.c */
void acquire(struct lock *);
void release(struct lock *);
int holding(struct lock *);
void pushcli();
void popcli();

/* page.c */
int mappages(uint32_t *pgdir, uint32_t vaddr, uint32_t size, uint32_t paddr, uint32_t flags);
pte_t *walkpgdir(uint32_t *pgdir, uint32_t vaddr, int isalloc);

/* prf.c */
void printf(char *fmt, ...);
void putchar(int c);
void panic(char *str);
void consoleintr(int (*getc)(void));
int consoleread(struct inode *ip, char *dst, int size);
int consolewrite(struct inode *ip, char *src, int size);
void consoleinit();

/* proc.c */
void uinit();
struct proc *palloc();
void pfree(struct proc *p);
int fdalloc(struct file *f);
void sched();
void sleep(void *wchan);
void wakeup(void *wchan);
void procdump();

/* string.c */
void memset(void *addr, int data, int count);
void memmove(void *src, void *dst, uint32_t count);
int strcmp(const char *p, const char *q, uint32_t limit); /* same = 1 */

/* swtch.S */
void swtch(struct context **_old, struct context *_new);

/* syscall.c */
int syscall(uint32_t num);

/* trap.c */
void idtinit();
void picinit();
void pitinit();

/* trapasm.S */
void trapret();

/* vm.c */
void seginit();
void kvminit();
uint32_t *setupkvm();
void inituvm(uint32_t *pgdir, void *start, uint32_t size);
int loaduvm(uint32_t *pgdir, char *addr, struct inode *ip, uint32_t offset, uint32_t size);
int allocuvm(uint32_t *pgdir, uint32_t oldsz, uint32_t newsz);
int deallocuvm(uint32_t *pgdir, uint32_t oldsz, uint32_t newsz);
void freevm(uint32_t *pgdir);
void switchuvm(struct proc *p);
