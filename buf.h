#pragma once

#include "lock.h"
#include "param.h"
#include "types.h"

#define NBUF 1024

struct buf {
    uint16_t flags;
    uint16_t refcnt;  /* refarence count */
    struct lock lock; /* is locked */
    uint32_t blockno;
    uint8_t data[SECTSIZE];
} buf[NBUF];

#define B_VALID 0x1 // buffer is valid
#define B_DIRTY 0x2 // buffer needs to be written to disk