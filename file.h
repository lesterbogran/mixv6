#pragma once

#include "lock.h"
#include "param.h"
#include "types.h"

#define NFILE 64
#define NINODE 64
#define NDEV 10
#define SUPERBLOCK 1
#define ROOTDIR 2
#define FSTART 2048 /* 0:boot, 1:super block, 2~2047:inode, 2048~:file system*/

struct sysfs {
    uint8_t nfree;  /* number of in core free blocks (0-127) */
    uint8_t ninode; /* number of in core I nodes (0-127) */
    uint16_t pad;
    uint32_t freeblock;  /* block of free list, last entry is pointer to next block. */
    uint32_t inodeblock; /* block of inode list, last entry is pointer to next block. */
};

#define T_DIR 1  /* Directory */
#define T_FILE 2 /* File */
#define T_DEV 3  /* Device */
struct dinode {
    uint16_t type;
    uint16_t uid;       /* user id */
    uint16_t gid;       /* group id */
    uint16_t mode;      /* permission */
    uint32_t nlink;     /* directory entries */
    uint32_t size;      /* file size */
    uint32_t addr[124]; /* device addresses constituting file.*/
};

#define FNAMESIZ 28
#define NDIRENT 16
struct directory {
    struct dir {
        uint32_t ino;
        char fname[FNAMESIZ];
    } dir[NDIRENT];
};

struct file {
    uint32_t refcnt;     /* reference count */
    struct inode *inode; /* pointer to inode structure */
    uint32_t offset;     /* read/write character pointer */
} file[NFILE];

struct inode {       /* sizeof(struct inode) = 512Byte */
    uint32_t num;    /* i number, 1-to-1 with device address */
    uint16_t refcnt; /* reference count */
    uint16_t valid;
    struct lock lock;
    uint32_t devno;

    /* copy from dinode */
    uint16_t type;
    uint16_t uid;       /* user id */
    uint16_t gid;       /* group id */
    uint16_t mode;      /* permission */
    uint32_t nlink;     /* directory entries */
    uint32_t size;      /* file size */
    uint32_t addr[124]; /* device addresses constituting file.*/
} inode[NINODE];

struct devsw {
    int (*read)(struct inode *, char *, int);
    int (*write)(struct inode *, char *, int);
} devsw[NDEV];

#define CONSOLE 1
#define STDIN 0
#define STDOUT 1
