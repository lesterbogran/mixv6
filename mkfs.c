#include "file.h"
#include "param.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

struct sector {
    int buf[SECTSIZE / 4];
};

int main(int argc, char const *argv[])
{
    struct sector *fs         = (struct sector *)malloc(DISKSIZE);
    struct sysfs *superblock  = (struct sysfs *)&fs[1];
    struct dinode *inodeblock = (struct dinode *)&fs[2];
    struct stat st;
    struct directory *rootdir;
    FILE *wf, *rf;
    int n = FSTART, t, fb;

    memset(fs, 0, DISKSIZE);

    /* set bootblock */
    rf = fopen("bootblock", "rb");
    fread(&fs[0], 1, SECTSIZE, rf);
    fclose(rf);

    /* set kernel */
    stat("kernel", &st);
    rf = fopen("kernel", "rb");
    t  = (st.st_size + SECTSIZE - 1) / SECTSIZE;
    fread(&fs[n], 1, t * SECTSIZE, rf);
    fclose(rf);
    n += t;

    /* set rootdir */
    rootdir               = (struct directory *)&fs[n];
    inodeblock[0].type    = T_DIR;
    inodeblock[0].mode    = 0x755;
    inodeblock[0].addr[0] = n++;

    /* set files */
    for (int i = 1; i < argc; i++) {
        // printf("file: %s add.\n", argv[i]);
        if (stat(argv[i], &st)) {
            fprintf(stderr, "Can't find file: %s\n", argv[i]);
            exit(1);
        };

        inodeblock[i].type = T_FILE;
        inodeblock[i].mode = 0x755;
        inodeblock[i].size = st.st_size;

        /* TODO: deal with filesize > 60KB */
        if (st.st_size > 124 * SECTSIZE) {
            fprintf(stderr, "Not support filesize > 62KB yet.");
            exit(1);
        }

        /* TODO: deal with filenum > 16 */
        if (i > 15) {
            fprintf(stderr, "Not support filenum > 15 yet.");
            exit(1);
        }

        rootdir->dir[i - 1].ino = i + 2;
        if (argv[i][0] == '_') {
            strncpy(rootdir->dir[i - 1].fname, &argv[i][1], 28);
        }
        else {
            strncpy(rootdir->dir[i - 1].fname, argv[i], 28);
        }

        rf = fopen(argv[i], "rb");
        for (int j = 0; j < 124; j++) {
            if (fread(&fs[n], 1, SECTSIZE, rf) <= 0) {
                break;
            }
            inodeblock[i].addr[j] = n++;
        }
        fclose(rf);
        inodeblock[0].size += sizeof(struct dir);
    }

    /* set free block */
    fb                    = n++;
    t                     = 0;
    superblock->freeblock = fb;
    superblock->nfree     = 0;
    for (int i = n; i < NSECTOR; i++) {
        fs[fb].buf[t] = n++;
        if (t == 127) {
            fb = fs[fb].buf[t];
            t  = 0;
            continue;
        }
        t++;
    }

    /* set inode block */
    for (int i = 0; i < FSTART; i++) {
        if (inodeblock[i].type == 0) {
            fb = n = i + 2;
            break;
        }
    }
    t                      = 0;
    superblock->inodeblock = n++;
    superblock->ninode     = 0;
    for (int i = n; i < FSTART; i++) {
        fs[fb].buf[t] = n++;
        if (t == 127) {
            fb = fs[fb].buf[t];
            t  = 0;
            continue;
        }
        t++;
    }
    // printf("superblock.inodeblock = 0x%x\n", superblock->inodeblock);
    // printf("superblock.freeblock = 0x%x\n", superblock->freeblock);

    wf = fopen("mixv6.img", "wb");
    fwrite(fs, 1, DISKSIZE, wf);
    fclose(wf);

    free(fs);
    return 0;
}
