#include "vm.h"
#include "asm.h"
#include "defs.h"
#include "macro.h"
#include "page.h"
#include "param.h"
#include "proc.h"
#include "types.h"
#include "x86.h"

static uint16_t gdtr[3];

void seginit(void)
{

    gdt[SEG_KCODE >> 3] = SEG(STA_X | STA_R, 0, 0xffffffff, 0);
    gdt[SEG_KDATA >> 3] = SEG(STA_W, 0, 0xffffffff, 0);
    gdt[SEG_UCODE >> 3] = SEG(STA_X | STA_R, 0, 0xffffffff, DPL_USER);
    gdt[SEG_UDATA >> 3] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
    gdtr[0]             = sizeof(gdt) - 1;
    gdtr[1]             = (uint32_t)gdt;
    gdtr[2]             = (uint32_t)gdt >> 16;
    asm volatile("lgdt (%0)" ::"r"(gdtr));
    printf("Set GDT ... Done.\n");
}

extern char kernel_end[]; /* first address after kernel loaded from ELF file */
extern char rodata_end[]; /* address of finish read only data */

static struct kmap {
    uint32_t start;
    uint32_t end;
    uint32_t flags;
} kmap[3];

void kmapinit(struct kmap *k, uint32_t start, uint32_t end, uint32_t flags)
{
    k->start = start;
    k->end   = end;
    k->flags = flags;
}

void kvminit()
{
    kmapinit(&kmap[0], 0, EXTMEM, PTE_W);                     /* I/O space */
    kmapinit(&kmap[1], EXTMEM, (uint32_t)rodata_end, 0);      /* kern text+rodata */
    kmapinit(&kmap[2], (uint32_t)rodata_end, MEMSIZE, PTE_W); /* kern data+memory */
    printf("Set Memory Map ... Done.\n");
}

uint32_t *setupkvm()
{
    uint32_t *pgdir;

    if ((pgdir = kalloc()) == 0) {
        return 0;
    }
    memset(pgdir, 0, PAGESIZE);

    for (int i = 0; i < NELEM(kmap); i++) {
        /* kernel space is static, so paddr is not used kalloc() */
        if (mappages(pgdir, kmap[i].start, kmap[i].end - kmap[i].start, kmap[i].start, kmap[i].flags) < 0) {
            freevm(pgdir);
            return 0;
        }
    }
    return pgdir;
}

/* initcode space */
void inituvm(uint32_t *pgdir, void *start, uint32_t size)
{
    if (size >= PAGESIZE) {
        panic("inituvm");
    }
    char *paddr = kalloc();
    memset(paddr, 0, PAGESIZE);
    mappages(pgdir, KERNEND, PAGESIZE, (uint32_t)paddr, PTE_W | PTE_U);
    memmove(start, paddr, size);
}

int loaduvm(uint32_t *pgdir, char *addr, struct inode *ip, uint32_t offset, uint32_t size)
{
    uint32_t pa, n;
    pte_t *pte;

    if ((uint32_t)addr % PAGESIZE != 0)
        panic("loaduvm: addr must be page aligned");
    for (int i = 0; i < size; i += PAGESIZE) {
        if ((pte = walkpgdir(pgdir, (uint32_t)addr + i, 0)) == 0)
            panic("loaduvm: address should exist");
        pa = PTE_ADDR(*pte);
        n  = (size - i < PAGESIZE) ? size - i : PAGESIZE;
        if (readi(ip, (char *)pa, offset + i, n) != n)
            return -1;
    }
    return 0;
}

/* oldsize = KERNEND + USERSIZE */
int allocuvm(uint32_t *pgdir, uint32_t oldsz, uint32_t newsz)
{
    char *paddr;
    uint32_t vaddr;

    if (newsz >= USEREND)
        return 0;
    if (newsz < oldsz)
        return oldsz;

    vaddr = PGROUNDUP(oldsz);
    for (; vaddr < newsz; vaddr += PAGESIZE) {
        paddr = kalloc();
        if (paddr == 0) {
            printf("allocuvm out of memory\n");
            deallocuvm(pgdir, newsz, oldsz);
            return 0;
        }
        memset(paddr, 0, PAGESIZE);
        if (mappages(pgdir, vaddr, PAGESIZE, (uint32_t)paddr, PTE_W | PTE_U) < 0) {
            printf("allocuvm out of memory (2)\n");
            deallocuvm(pgdir, newsz, oldsz);
            kfree(paddr);
            return 0;
        }
    }
    return newsz;
}

int deallocuvm(uint32_t *pgdir, uint32_t oldsz, uint32_t newsz)
{
    pte_t *pte;
    uint32_t vaddr, paddr;

    if (newsz >= oldsz) {
        return oldsz;
    }

    vaddr = PGROUNDUP(newsz);
    for (; vaddr < oldsz; vaddr += PAGESIZE) {
        pte = walkpgdir(pgdir, vaddr, 0);
        // pte = 0 -> no page directory
        // *pte = 0 -> no page table
        if (pte == 0) {
            vaddr = PGADDR(PDX(vaddr) + 1, 0, 0) - PAGESIZE; /* end this loop, vaddr += PAGESIZE */
        }
        else if (*pte & PTE_P) { /* pgdir is exist and pgtable have P flags */
            paddr = PTE_ADDR(*pte);
            if (paddr == 0) {
                panic("deallocuvm");
            }
            kfree((char *)paddr);
            *pte = 0;
        }
    }
    return newsz;
}

void freevm(uint32_t *pgdir)
{
    if (pgdir == 0) {
        panic("freevm: no pgdir");
    }
    deallocuvm(pgdir, USEREND, KERNEND);
    for (int i = 256; i < NPDENT; i++) {
        if (pgdir[i] & PTE_P) {
            kfree((char *)PTE_ADDR(pgdir[i]));
        }
    }
    kfree((char *)pgdir);
}

/* trap from userspace (cross ring)(ring 11 -> ring 00), 
 * %esp will be set address that Task Register's esp0. 
 */
void switchuvm(struct proc *p)
{
    if (p == 0)
        panic("switchuvm: no process");
    if (p->kstack == 0)
        panic("switchuvm: no kstack");
    if (p->pgdir == 0)
        panic("switchuvm: no pgdir");

    pushcli();
    gdt[SEG_TSS >> 3]   = SEG16(STS_T32A, &ts, sizeof(ts) - 1, 0);
    gdt[SEG_TSS >> 3].s = 0;
    ts.ss0              = SEG_KDATA;
    ts.esp0             = (uint32_t)p->kstack + PAGESIZE;
    // setting IOPL=0 in eflags *and* iomb beyond the tss segment limit
    // forbids I/O instructions (e.g., inb and outb) from user space
    ts.iomb = (uint16_t)0xFFFF;
    asm volatile("ltr %0" ::"r"((uint16_t)SEG_TSS));
    scr3(p->pgdir); // switch to process's address space
    popcli();
}