#pragma once

typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;
typedef uint32_t pde_t;
typedef uint32_t pte_t;

/* buf.h */
struct buf;

/* elf.h */
struct elfhdr;
struct proghdr;

/* file.h */
struct file;
struct inode;

/* fs.h */
struct sysfs;
struct dinode;
struct directory;

/* lock.h */
struct lock;

/* page.h */
struct map;
struct mmap;

/* proc.h */
struct proc;
struct user;
struct context;

/* trap.h */
struct trapframe;
struct gatedesc;