#pragma once

#include "param.h"
#include "types.h"

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

#define PGROUNDUP(sz) (((sz) + PAGESIZE - 1) & ~(PAGESIZE - 1))   /* round up to page size */
#define PGROUNDDOWN(a) (((a)) & ~(PAGESIZE - 1))                  /* round down to page size */
#define PDX(va) (((uint32_t)(va) >> 22) & 0x3FF)                  /* page directory index */
#define PTX(va) (((uint32_t)(va) >> 12) & 0x3FF)                  /* page table index */
#define PGADDR(d, t, o) ((uint32_t)((d) << 22 | (t) << 12 | (o))) /* construct virtual address from indexes and offset */

/* Address in page table or page directory entry */
#define PTE_ADDR(pte) ((uint32_t)(pte) & ~0xFFF)
#define PTE_FLAGS(pte) ((uint32_t)(pte)&0xFFF)

#define SECTROUNDUP(sz) (((sz) + SECTSIZE - 1) & ~(SECTSIZE - 1)) /* round up to sector size */
#define SIZE2SECTN(sz) (((sz) + SECTSIZE - 1) / SECTSIZE)         /* round up to sector size */

/* number of elements in fixed-size array */
#define NELEM(x) (sizeof(x) / sizeof((x)[0]))
