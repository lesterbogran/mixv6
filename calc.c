#include "ulib.h"

int main(void)
{
    volatile unsigned int f2;
    for (volatile int i = 0; i < 0x100000; i++) {
        volatile unsigned int f0 = 0, f1 = 1;
        while (f1 < 0x80000000) {
            f2 = f1 + f0;
            f0 = f1;
            f1 = f2;
        }
    }
    printf("ans:%x\n", f2);
    exit();
    return 0;
}
