
// system calls
int open(const char *path);
int close(int fd);
int read(int fd, void *dst, int size);
int write(int fd, const void *src, int size);
int exec(char *path);
int exit(void);
int wait(void);

// uprf.c
void printf(char *fmt, ...);

// ustring.c
void memset(char *addr, char data, int count);
void memmove(char *src, char *dst, int count);
int strcmp(const char *p, const char *q, int limit);
int isbackground(char *cmd, char pat, int size);
